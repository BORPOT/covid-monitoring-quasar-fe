import { QSpinnerRadio } from 'quasar'

export default {
  methods: {
    showLoading (message) {
      this.$q.loading.show({
        message: message
      })
    },
    hideLoading () {
      this.$q.loading.hide()
    }
  },
  created () {
    this.$q.loading.setDefaults({
      spinner: QSpinnerRadio,
      spinnerColor: 'warning'
    })
  }
}
