
export default {
  methods: {
    errorNotif (message) {
      this.$q.notify({
        message,
        icon: 'error',
        color: 'negative',
        textColor: 'white',
        multiLine: true,
        actions: [
          { label: 'Dismiss', color: 'white' }
        ]
      })
    },
    successNotif (message) {
      this.$q.notify({
        multiLine: true,
        message,
        progress: true,
        icon: 'done_all',
        color: 'positive',
        textColor: 'dark'
      }, 2000)
    }
  }
}
