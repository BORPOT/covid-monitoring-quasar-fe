import { auth } from 'boot/firebase'

export default {
  namespaced: true,
  state: {
    authedUser: {},
    userSignedIn: false
  },
  actions: {
    async signUserIn ({ commit, dispatch, rootGetters }, payload) {
      console.log('signing user in')

      await auth.signInWithEmailAndPassword(payload.email, payload.password)
        .then(async () => {
          const user = auth.currentUser
          await dispatch('users/retrieveUser', user.uid, { root: true }).then(() => {
            console.log('user is signed in', rootGetters['users/user'])
            commit('setAuthedUser', rootGetters['users/user'])
            commit('setUserSignedIn', true)
          })
          return new Promise((resolve) => {
            resolve()
          })
        })
        .catch(err => {
          return new Promise((resolve, reject) => {
            reject(err)
            console.log(err)
            commit('setUserSignedIn', false)
          })
        })
    },
    async signUserUp ({ commit, dispatch, rootGetters }, payload) {
      console.log('Signing User up: ', payload)

      const { firstName, lastName, email, password, displayName } = payload
      // const { email, password, displayName } = payload

      await auth.createUserWithEmailAndPassword(email, password)
        .then(async () => {
          const user = auth.currentUser
          // console.log('User is Signed Up', user)
          
          await auth.currentUser.updateProfile({ displayName }).then(async () => {
            // commit('setAuthedUser', user.user)
            await dispatch('users/createUser', {
              id: user.uid,
              data: {
                firstName,
                lastName,
                email,
                displayName
              }
            }, { root: true }).then(() => {
              commit('setAuthedUser', rootGetters['users/user'])
            })
            return new Promise((resolve) => {
              resolve()
            })
          })
        })
        .catch(err => {
          return new Promise((resolve, reject) => {
            reject(err)
            console.log(err)
            commit('setUserSignedIn', false)
          })
        })
    },
    signUserOut ({ commit }) {
      console.log('signing user out')

      auth.signOut()
        .then(() => {
          console.log('user is signed out')
          commit('setAuthedUser', {})
          commit('setUserSignedIn', false)
        })
        .catch(err => {
          console.log(err)
          commit('setUserSignedIn', false)
        })
    },
    handleAuthStateChanged ({ dispatch, rootGetters, commit }) {
      auth.onAuthStateChanged(async user => {
        if (user) {
          const id = auth.currentUser.uid
          await dispatch('users/retrieveUser', id, { root: true })
          // console.log('user is signed in', rootGetters['users/user'], rootGetters)
          commit('setAuthedUser', rootGetters['users/user'])
          commit('setUserSignedIn', true)
        } 
        else {
          commit('setAuthedUser', {})
          commit('setUserSignedIn', false)
        }
      })
    }
  },
  mutations: {
    setAuthedUser (state, payload) {
      state.authedUser = payload
    },
    setUserSignedIn (state, payload) {
      state.userSignedIn = payload
    }

  },
  getters: {
    authedUser (state) {
      return state.authedUser
    },
    userSignedIn (state) {
      return state.userSignedIn
    }
  }
}