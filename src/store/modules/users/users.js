import { firestoreHelper } from 'src/store/helpers/fstore'

const users =  {
  namespaced: true,
  state: {
    ...firestoreHelper.state
  },
  mutations: {
    ...firestoreHelper.mutations
  },
  actions: {
    ...firestoreHelper.actions,
    async createUser ({ commit, dispatch }, payload) {
      const { id, data } = payload
      commit('setColName', 'users')
      await dispatch('createDoc', { id, data })
    },
    async retrieveUser ({ commit, dispatch }, payload) {
      const id  = payload
      commit('setColName', 'users')
      console.log("USERS: Retrieving Document with id ", id)
      await dispatch('retrieveDoc', id)
    }
  },
  getters: {
    user (state) {
      return state.doc
    }
  }
}

console.log(users)

export default users