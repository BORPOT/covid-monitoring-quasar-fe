import { db } from 'boot/firebase'

export const firestoreHelper = {
  state: {
    colName: '',
    col: null,
    docName: '',
    doc: {},
    docId: null
  },
  mutations: {
    setColName (state, payload) {
      state.colName = payload
    },
    setDocId (state, payload) {
      state.docId = payload
    },
    setDoc (state, payload) {
      Object.assign(state.doc, payload)
    }
  },
  actions: {
    async createDoc ({ commit, state }, payload) {
      const { id, data } = payload
      console.log(`creating ${state.docName} document`, payload)
                
      let t
      if (id) t = db.collection(state.colName).doc(id).set(data)
      else t = db.collection(state.colName).add(data)
                
      await t.then(() => {
        // console.log('thedoc: ', doc)
        commit('setDocId', id)
        commit('setDoc', data)
        console.log(`${state.colName} document is created: ${state.doc}`)
      })
        .catch(err => {
          console.log(`error in document creation: ${err}`)
        })
    },
    async retrieveDoc ({ commit, state }, payload) {
      const id = payload
                
      const docRef = db.collection(state.colName).doc(id)
                
      await docRef.get().then(doc => {
        if (doc.exists) {
          commit('setDocId', id)  
          commit('setDoc', doc.data())
          console.log('document is retrieved:', doc.data())
        }
        else {
          console.log('No Such Document.')
        }
      })
        .catch(error => {
          console.log("Error getting document:", error)
        })
    }
  }
}