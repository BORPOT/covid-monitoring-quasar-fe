
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'sign-in', name: 'sign-in', component: () => import('pages/SignIn.vue'), meta: { requiresGuest: true } },
      { path: 'sign-up', name: "sign-up", component: () => import('pages/SignUp.vue'), meta: { requiresGuest: true } },
      { path: 'establishments', name: "establishments", component: () => import('pages/Establishments.vue') },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
