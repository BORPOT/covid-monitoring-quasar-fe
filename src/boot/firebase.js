// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from "firebase/app"

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
// import "firebase/analytics"

// Add the Firebase products that you want to use
import "firebase/auth"
// import "firebase/database"
import "firebase/firestore"

var firebaseConfig = {
  apiKey: "AIzaSyDTsi73ucron33udEvkqf7f90Z_8AjtZh0",
  authDomain: "covid-monitoring-iit.firebaseapp.com",
  databaseURL: "https://covid-monitoring-iit.firebaseio.com",
  projectId: "covid-monitoring-iit",
  storageBucket: "covid-monitoring-iit.appspot.com",
  messagingSenderId: "891619650563",
  appId: "1:891619650563:web:eabb6aa83b80830a15f236"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig)

const db = firebase.firestore()
const auth = firebase.auth()
// firebase.analytics()

export { auth, db }