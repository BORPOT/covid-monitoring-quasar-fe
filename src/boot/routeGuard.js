import { Notify } from 'quasar'

export default ({ router, store, Vue }) => {
  router.beforeEach(async (to, from, next) => {
    // Now you need to add your authentication logic here, like calling an API endpoint
  
    console.log("Route Guard Instantiating!")
    console.log("Checking if User is Signed In!")
    await store.dispatch('userAuth/handleAuthStateChanged', { root: true }).then(() => {
      console.log(store)
      const userSignedIn = store.getters['userAuth/userSignedIn']
      console.log("Sign in Status ", userSignedIn)
      if (to.matched.some(record => record.meta.requiresAuth)) {
        if (userSignedIn) next()
        else {
          next({ name: 'sign-in', query: { redirect: to.fullPath } }) 
          Notify.create({
            multiLine: true,
            message: 'User Authorization Required!',
            caption: 'Please Login to continue',
            progress: true,
            icon: 'error',
            color: 'negative',
            textColor: 'white'
          }, 2000)
        }
      } else if (to.matched.some(record => record.meta.requiresGuest)) {
        if (userSignedIn) next({ name: 'landing-page' })
        else next()
      }
      else {
        next()
      }
    })
  })
}